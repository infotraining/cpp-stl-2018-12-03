#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <stack>

#include "catch.hpp"

using namespace std;

TEST_CASE("stack")
{
    stack<int> s;

    s.push(1);
    s.push(2);

    for(int i = 3; i < 10; ++i)
        s.push(i);

    while(!s.empty())
    {
        //int item = s.pop();

        int item = s.top();
        s.pop();

        cout << item << '\n';
    }

    
}