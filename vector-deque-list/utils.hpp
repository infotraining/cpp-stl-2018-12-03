#include <iostream>
#include <string>
#include <string_view>

namespace Utils
{
    template <typename Container>
    void print(const Container& container, std::string_view prefix)
    {
        std::cout << prefix << ": [ ";
        for (const auto& item : container)
            std::cout << item << " ";
        std::cout << "]" << std::endl;
    }


    class Gadget
    {
        int id_;
        std::string name_;

    public:
        static int gen_id()
        {
            static int id_seed;
            return ++id_seed;
        }

        Gadget()
            : id_{gen_id()}
            , name_{"unknown"}
        {
            std::cout << "Gadget(" << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget(int id, const std::string& name = "unknown")
            : id_{id}
            , name_{name}
        {
            std::cout << "Gadget(" << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget(const Gadget& source)
            : id_{source.id_}
            , name_{source.name_}
        {
            std::cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget(Gadget&& source) noexcept
            : id_{source.id_}
            , name_{std::move(source.name_)}
        {
            std::cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget& operator=(const Gadget& source)
        {
            if (this != &source)
            {
                id_ = source.id_;
                name_ = source.name_;
            }

            return *this;
        }

        Gadget& operator=(Gadget&& source)
        {
            if (this != &source)
            {
                id_ = source.id_;
                name_ = std::move(source.name_);
            }

            return *this;
        }

        int id() const
        {
            return id_;
        }

        std::string name() const
        {
            return name_;
        }
    };

    inline std::ostream& operator<<(std::ostream& out, const Gadget& g)
    {
        out << "Gadget{id: " << g.id() << ", name: " << g.name() << "}";
        return out;
    }
}

class Wrapper
{
    Utils::Gadget g;

    Wrapper(Wrapper&&) = default; // implicitly noexcept 

    virtual ~Wrapper() = default;
};