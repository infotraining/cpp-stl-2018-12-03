#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <deque>
#include <list>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

template <typename T>
void print_stat(const std::vector<T>& vec, std::string_view prefix)
{
    std::cout << prefix << " - size: " << vec.size() 
              << " - capacity: " << vec.capacity() << '\n';
}

TEST_CASE("vector")
{
    SECTION("default constructor")
    {
        vector<int> vec;

        print(vec, "vec");
        print_stat(vec, "vec");
    }

    SECTION("constructor with size")
    {
        vector<int> vec(10);
        print(vec, "vec");

        vector<Gadget> gadgets(5);
        print(gadgets, "gadgets");
    }

    SECTION("construct from initializer list")
    {
        vector vec{ 1, 2, 3, 4, 5 };

        print(vec, "vec deduced");
    }

    SECTION("constuct from range")
    {
        array arr = {1, 2, 3, 4, 5 };

        vector vec(begin(arr), end(arr));
        print(vec, "vec");
    }
}

TEST_CASE("efficient erase")
{
    vector vec = { 1, 2, 3, 4, 5, 6 };
    auto pos = vec.begin() + 2;

    SECTION("linear time")
    {
        vec.erase(pos);

        REQUIRE(vec == vector{ 1, 2, 4, 5, 6 });
    }

    SECTION("constant time - but order of items will change")
    {        
        *pos = std::move(vec.back());        
        vec.pop_back();
    }
}

TEST_CASE("push_backs")
{
    vector<int> vec;
    vec.reserve(32);

    print_stat(vec, "vec");

    for(int i = 0; i < 33; ++i)
    {
        vec.push_back(i);
        print_stat(vec, "vec");
    }

    vec.resize(4);
    vec.shrink_to_fit();
    print(vec, "vec");
    print_stat(vec, "vec");
}

struct X
{
    string text;
};

TEST_CASE("safety for exceptions")
{
    vector<Gadget> vec;

    vec.push_back(Gadget(1, "ipad"));
    vec.push_back(Gadget(2, "smartphone"));
    vec.push_back(Gadget(3, "pendrive"));
    vec.push_back(Gadget(4, "gadget"));
    
    for(int i = 5; i < 16; ++i)
    {
        vec.push_back(Gadget(i, "gadget " + to_string(i))); // if bad thing happen i won't loose data
    }
}

TEST_CASE("vector<bool>")
{
    vector<bool> vec_bits = {1, 0, 0, 1, 1, 0};

    REQUIRE(vec_bits[0]);

    vec_bits.flip();

    print(vec_bits, "vec_bits");

    vector<int> vec = { 1, 2, 3 };

    int* ptr_i = &vec[0];

    // bool* ptr_b = &vec_bits[0]; // ERROR

    for(auto&& item : vec_bits) // auto&& required for vector<bool>
    {
        item.flip();
    }

    print(vec_bits, "vec_bits");
}

TEST_CASE("deque")
{
    deque dq = {1, 2, 3, 4, 5};

    auto& ref_item = dq[2];

    dq.push_back(6);
    dq.push_front(0);

    REQUIRE(dq.size() == 7);
    REQUIRE(ref_item == 3);
}

TEST_CASE("list")
{
    list<int> lst1 = {1, 2, 3, 4, 5, 6};
    list lst2 = { 1, 5, 8, 10 };

    lst1.merge(lst2);

    print(lst1, "lst1");
    print(lst2, "lst2");

    lst1.unique();
    lst1.remove_if([](int x) { return x % 2 == 0;});
    print(lst1, "lst1");
}