#include <algorithm>
#include <iostream>
#include <numeric>
#include <set>
#include <string>
#include <vector>
#include <map>
#include <boost/bimap.hpp>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

TEST_CASE("sets")
{
    set<int> set_int = {5, 2, 5, 1234, 2, 665, 42, 1, 54, 6};

    print(set_int, "set_int");

    if (auto pos = set_int.find(42); pos != set_int.end())
    {
        cout << "Item is found: " << *pos << '\n';
    }

    set_int.erase(665);

    print(set_int, "set_int");

    if (auto [pos, inserted] = set_int.insert(777); inserted)
    {
        cout << *pos << " was inserted into container\n";
    }
    else
    {
        cout << *pos << " was already in a container\n";
    }

    set_int.emplace_hint(set_int.end(), 2018);

    print(set_int, "set_int");
}

TEST_CASE("multiset")
{
    multiset ms = {1, 2, 3, 3, 3, 5, 6};

    auto lb = ms.lower_bound(3);
    auto ub = ms.upper_bound(3);

    auto [eq1, eq2] = ms.equal_range(3);

    REQUIRE(lb == eq1);
    REQUIRE(ub == eq2);

    for (auto it = lb; it != ub; ++it)
    {
        cout << *it << " ";
    }
    cout << '\n';

    auto [pos1, pos2] = ms.equal_range(4);

    REQUIRE(pos1 == pos2);
    ms.insert(pos1, 4);

    print(ms, "ms");
}

bool ptr_less_compare(const unique_ptr<int>& a, const unique_ptr<int>& b)
{
    return *a < *b;
}

TEST_CASE("sorting functors")
{
    SECTION("as template parameters")
    {
        multiset<int, greater<int>> msg = {5, 24, 12, 4, 1235, 1, 42};

        print(msg, "msg");
    }

    SECTION("as constructor parameter - function pointer")
    {
        multiset<
            unique_ptr<int>, 
            bool(*)(const unique_ptr<int>&, const unique_ptr<int>&)
        > ms_unique_ptrs(&ptr_less_compare);

        ms_unique_ptrs.insert(make_unique<int>(5));
        ms_unique_ptrs.insert(make_unique<int>(1));
        ms_unique_ptrs.insert(make_unique<int>(4));
        ms_unique_ptrs.insert(make_unique<int>(8));

        for(const auto& up : ms_unique_ptrs)
            cout << *up << " ";
        cout << '\n';
    }

    SECTION("as constructor parameter - function pointer")
    {
        auto compare_ptr_less = [](const auto& a, const auto& b) { return *a < *b; };

        multiset<unique_ptr<int>, decltype(compare_ptr_less)> ms_unique_ptrs(compare_ptr_less);

        ms_unique_ptrs.insert(make_unique<int>(5));
        ms_unique_ptrs.insert(make_unique<int>(1));
        ms_unique_ptrs.insert(make_unique<int>(4));
        ms_unique_ptrs.insert(make_unique<int>(8));

        for(const auto& up : ms_unique_ptrs)
            cout << *up << " ";
        cout << '\n';
    }
}

TEST_CASE("maps")
{
    map<int, string> dict = { {1, "one"}, {2, "dwa"}, {5, "five"}, {3, "three"} };

    print(dict, "dict");

    dict.insert(pair(7, "seven"));
    dict.insert_or_assign(2, "two");
    dict.insert_or_assign(6, "six");

    for(const auto&[key, value] : dict)
    {
        cout << "[ " << key << ", " << value << "]" << " ";
    }
    cout << "\nn";

    ////////////////////////////////////////////////////////////////

    using BiMap = boost::bimap<int, string>; 
    BiMap bimap_dict;

    for(const auto&[key, value] : dict)
    {
        bimap_dict.insert(BiMap::value_type(key, value));
    }

    auto& left = bimap_dict.left;
    auto& right = bimap_dict.right;

    for(auto&& p : bimap_dict.left)
    {
        cout << p.first << " - " << p.second << endl;
    }    
        
    cout << "\n";

    for(auto it = right.begin(); it != right.end(); ++it)
    {
        cout << it->first << " - " << it->second << endl;
    }
    cout << "\n";
}