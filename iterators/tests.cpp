#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <random>
#include <iterator>
#include <list>
#include <numeric>

#include "catch.hpp"

using namespace std;

template <typename InpIt, typename OutIt, typename Predicate>
auto my_copy_if(InpIt first, InpIt last, OutIt out, Predicate predicate)
{
    for(auto it = first; it != last; ++it)
    {
        if (predicate(*it))
        {
            *out = *it;            
            ++out;
        }
    }
}

TEST_CASE("iterators")
{    
    mt19937_64 rnd_gen(665);
    uniform_int_distribution<> rnd_distr(0, 100);

    array<int, 10> arr;
    generate(begin(arr), end(arr), 
             [&]() { return rnd_distr(rnd_gen); });

    vector<int> dest;
    copy_if(rbegin(arr), rend(arr), back_inserter(dest), [](int x) { return x % 2 == 0;});

    //REQUIRE(dest == vector{58, 14, 60, 84, 32});    
    REQUIRE(distance(begin(arr), end(arr)) == size(arr));
}

TEST_CASE("insert iterators")
{
    vector<int> vec;

    back_insert_iterator<vector<int>> bit(vec);

    *bit = 1; // vec.push_back(1)
    ++bit;
    *bit = 2; // vec.push_back(2)
    ++bit;
    *bit = 3; // vec.push_back(3)
    ++bit;

    REQUIRE(vec == vector{1, 2, 3});
}

// TEST_CASE("stream iterators")
// {
//     cout << "Enter items: \n";

//     istream_iterator<int> start_it(cin);
//     istream_iterator<int> end_it;
        
//     vector<int> vec(start_it, end_it);
//     copy(begin(vec), end(vec), ostream_iterator<int>(cout, " "));
    
//     cout << endl;
// }

template <typename InpIter, typename TResult>
TResult my_accumulate(InpIter first, InpIter last, TResult init)
{
    //typename iterator_traits<Iterator>::value_type result{};
    TResult result = init;

    for(auto it = first; it != last; ++it)
        result += *it;

    return result;
} 


TEST_CASE("bug")
{
    vector<double> data = { 1.1, 2.3, 4.5 };

    auto result = my_accumulate(begin(data), end(data), 0);
}

template <typename Iterator>
void my_advance(Iterator& it, size_t n)
{
    // auto tmp1 = *it;
    // typename iterator_traits<Iterator>::value_type tmp2{};
    // remove_reference_t<decltype(*it)> tmp3{};

    if constexpr (is_same_v<typename iterator_traits<Iterator>::iterator_category, random_access_iterator_tag>)
    {
        cout << "Optimize for r_a_it\n";
        it += n;
    }
    else
    {
        cout << "Generic impl\n";
        for(size_t i = 0; i < n; ++i)
            ++it;
    }
}

TEST_CASE("iterator_traits")
{
    list lst = { 1, 2, 3, 4, 5, 6, 7 };

    auto pos = lst.begin();

    my_advance(pos, 3);

    REQUIRE(*pos == 4);
}