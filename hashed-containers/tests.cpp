#include <algorithm>
#include <boost/functional/hash.hpp>
#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <string_view>
#include <unordered_set>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename HashContainer>
void print(const HashContainer& c, string_view prefix)
{
    cout << prefix << ": "
         << "size: " << c.size()
         << "; bucket_count: " << c.bucket_count()
         << "; max_load_factor: " << c.max_load_factor()
         << "; load_factor: " << c.load_factor() << endl;
}

TEST_CASE("hashed containers")
{
    unordered_set<int> uset_int(10);
    uset_int.max_load_factor(0.3);

    print(uset_int, "uset_int");

    random_device rd;
    mt19937_64 rnd_gen{rd()};
    uniform_int_distribution<> rnd_distr(1, 1000);

    for (int i = 0; i < 20; ++i)
    {
        uset_int.insert(rnd_distr(rnd_gen));
        print(uset_int, "uset_int");
    }

    cout << "\nBuckets: " << endl;

    for (size_t i = 0; i < uset_int.bucket_count(); ++i)
    {
        cout << "Bucket " << i << ": [ ";
        for (auto bit = uset_int.begin(i); bit != uset_int.end(i); ++bit)
        {
            cout << *bit << " ";
        }
        cout << "]\n";
    }

    SECTION("finding item")
    {
        if (auto pos = uset_int.find(13); pos != uset_int.end())
        {
            cout << "Item " << *pos << " has been found..." << endl;
        }
    }
}

template <typename... Args>
size_t get_hash_value(const Args&... value)
{
    size_t seed = 0;
    (boost::hash_combine(seed, value), ...);
    return seed;
}

template <typename Tpl, size_t... Is>
auto get_hash_for_tuple_impl(const Tpl& t, std::index_sequence<Is...>)
{
    return get_hash_value(std::get<Is>(t)...);
}

template <typename... Ts>
auto get_hash_for_tuple(const std::tuple<Ts...>& tpl)
{
    using Indexes = std::make_index_sequence<sizeof...(Ts)>;
    return get_hash_for_tuple_impl(tpl, Indexes{});
}

struct Person
{
    string first_name;
    string last_name;
    uint8_t age;

    auto tied() const
    {
        return tie(first_name, last_name, age);
    }

    bool operator==(const Person& other) const
    {
        return tied() == other.tied();
    }
};

struct HashPerson
{
    size_t operator()(const Person& p) const
    {               
        return get_hash_for_tuple(p.tied());
    }
};

namespace std
{
    template <>
    struct hash<Person>
    {
        size_t operator()(const Person& p) const
        {
            size_t seed{};

            boost::hash_combine(seed, p.first_name);
            boost::hash_combine(seed, p.last_name);
            boost::hash_combine(seed, p.age);

            return seed;
        }
    };
}

TEST_CASE("custom hash")
{
    //unordered_set<Person, HashPerson> uset_person;
    unordered_set<Person> uset_person;

    uset_person.insert(Person{"Jan", "Kowalski", 33});
    uset_person.insert(Person{"Jan", "Kowalski", 32});
    uset_person.insert(Person{"Jan", "Kowalski", 31});

    if (auto pos = uset_person.find(Person{"Jan", "Kowalski", 32}); pos != uset_person.end())
    {
        cout << "Found: " << pos->first_name << " " << pos->last_name
             << " " << pos->age << endl;
    }
}