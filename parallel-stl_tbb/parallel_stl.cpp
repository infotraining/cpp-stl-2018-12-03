#include <algorithm>
#include <cctype>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <pstl/algorithm>
#include <pstl/execution>
#include <pstl/numeric>
#include <random>
#include <string>

using namespace std;

template <typename F>
class Stoper
{
    std::string description_;
    std::chrono::high_resolution_clock::time_point start_;
    F func_;

public:
    template <typename Func>
    Stoper(const std::string& description, Func&& func)
        : description_{description}
        , func_{std::forward<Func>(func)}
    {
        start_ = std::chrono::high_resolution_clock::now();
        func_();
    }

    Stoper(const Stoper&) = delete;
    Stoper& operator=(const Stoper&) = delete;

    Stoper(Stoper&&) = default;
    Stoper& operator=(Stoper&&) = default;

    ~Stoper()
    {
        auto end = std::chrono::high_resolution_clock::now();

        auto time_interval = std::chrono::duration_cast<std::chrono::milliseconds>(end - start_);

        std::cout << description_ << " - time: " << time_interval.count() << "ms" << std::endl;
    }
};

template <typename Func>
Stoper(const std::string& description, Func&& func)->Stoper<decay_t<Func>>;

template <typename Func>
auto BENCHMARK(const std::string& description, Func&& func)
{
    return Stoper(description, std::forward<Func>(func));
}

namespace fs = std::filesystem;

std::optional<std::string> load_file_content(const std::filesystem::path& filename)
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);

    if (in)
    {
        std::string content;
        in.seekg(0, std::ios::end);
        content.resize(static_cast<size_t>(in.tellg()));
        in.seekg(0, std::ios::beg);
        in.read(&content[0], content.size());
        in.close();
        return content;
    }

    else
        return {};
}

template <typename ExecutionPolicy>
std::uintmax_t count_words(string_view text, ExecutionPolicy exec_policy)
{
    if (text.empty())
        return 0;

    auto is_word_beginning = [](unsigned char left, unsigned char right) {
        return std::isspace(left) && !std::isspace(right);
    };

    std::uintmax_t wc = (!std::isspace(text.front()) ? 1 : 0);
    wc += std::transform_reduce(exec_policy,
        text.begin(),
        text.end() - 1,
        text.begin() + 1,
        std::size_t(0),
        std::plus<>(),
        is_word_beginning);

    return wc;
}

int main()
{
    vector<int> vec(10'000'000);

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<int> rnd_distr(0, 100);

    generate(vec.begin(), vec.end(), [&] { return rnd_distr(rnd_gen); });

    unsigned long long sum1{};

    BENCHMARK("accumulate - normal", [&]() {
        sum1 = accumulate(vec.begin(), vec.end(), 0ULL);
    });

    std::cout << "sum1: " << sum1 << endl;

    unsigned long long sum2{};

    BENCHMARK("accumulate - parallel_unsequenced", [&]() {
        sum2 = reduce(std::execution::par_unseq, vec.begin(), vec.end(), 0ULL);
    });

    std::cout << "sum2: " << sum2 << endl;

    cout << "\n---------------------------------------\n";

    auto vec1 = vec;

    BENCHMARK("sorting - normal", [&] {
        sort(vec1.begin(), vec1.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    auto vec2 = vec;

    BENCHMARK("sorting - seqenced", [&] {
        sort(std::execution::seq, vec2.begin(), vec2.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    auto vec3 = vec;

    BENCHMARK("sorting - parallel_unseq", [&] {
        sort(std::execution::par_unseq, vec3.begin(), vec3.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    cout << "\n---------------------------------------\n";

    fs::path user_path = R"(/home)";
    vector<fs::directory_entry> dir_entries(fs::recursive_directory_iterator(user_path), {});

    uint64_t total_size1{};

    const string test_name = "size of directory: "s + user_path.c_str();

    BENCHMARK(test_name, [&] {
        total_size1 = accumulate(dir_entries.begin(), dir_entries.end(), 0ULL,
            [](const auto& ts, const fs::directory_entry& de) {
                auto fsize = fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u;
                return ts + fsize;
            });
    });

    std::cout << "total_size1: " << total_size1 << endl;

    uint64_t total_size2{};

    BENCHMARK(test_name + " - parallel", [&] {
        vector<uint64_t> file_sizes(dir_entries.size());

        transform(std::execution::par, dir_entries.begin(), dir_entries.end(), file_sizes.begin(),
            [](const fs::directory_entry& de) { return fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u; });

        total_size2 = reduce(std::execution::par_unseq, file_sizes.begin(), file_sizes.end(), 0ULL);
    });

    std::cout << "total_size2: " << total_size1 << endl;

    cout << "\n---------------------------------------\n";

    fs::path book{"./proust.txt"};

    auto content = load_file_content(book).value_or(""s);

    {
        uintmax_t word_count{};

        BENCHMARK("word count", [& content = as_const(content), &word_count] {
            word_count = count_words(content, std::execution::seq);
        });

        cout << "word_count: " << word_count << endl;
    }

    {
        uintmax_t word_count{};

        BENCHMARK("word count", [& content = as_const(content), &word_count] {
            word_count = count_words(content, std::execution::par_unseq);
        });

        cout << "word_count: " << word_count << endl;
    }
}