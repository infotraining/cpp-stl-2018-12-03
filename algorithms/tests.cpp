#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <list>
#include <random>
#include <iterator>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

template <typename T>
struct GreaterThan
{
    const T value;

    bool operator()(const T& item) const
    {
        return item > value;
    }
};


TEST_CASE("functors")
{
    vector vec = { 1, 2, 3, 4, 5, 6, 7, 8 };

    auto pos = find_if(begin(vec), end(vec), GreaterThan<int>{5});

    REQUIRE(pos != end(vec));
    REQUIRE(*pos == 6);    
}

TEST_CASE("stl functors")
{
    vector vec = { 1, 2, 3, 4, 5, 6, 7, 8 };    

    vector<int> dest(vec.size());

    transform(begin(vec), end(vec), rbegin(vec), begin(dest), plus<>{});

    print(vec, "vec1");
    print(dest, "dest");

    sort(begin(vec), end(vec), greater<>{});
    print(vec, "vec after sort");
}

TEST_CASE("lambdas")
{
    auto f = [](int x) { return x * 2; };

    vector vec = { 1, 2, 3, 4, 5, 6, 7, 8 };
    list<int> lst;

    transform(vec.begin(), vec.end(), back_inserter(lst), f);

    print(lst, "lst");
}

TEST_CASE("algorithms STL")
{
    vector vec = { 1, 3, 5, 7, 9, 11 };

    REQUIRE(none_of(begin(vec), end(vec), [](int x) { return x % 2 == 0; }));

    auto [pos1, pos2] = equal_range(begin(vec), end(vec), 6);

    REQUIRE(pos1 == pos2);

    vec.insert(pos1, 6);

    print(vec, "vec"); 

    random_device rd;
    mt19937_64 rnd_gen(rd());
    shuffle(vec.begin(), vec.end(), rnd_gen);

    print(vec, "vec after shuffle");  

    auto new_end = remove_if(vec.begin(), vec.end(), [](int x) {  return x % 2 != 0; });
    vec.erase(new_end, vec.end());

    print(vec, "vec after remove");  
}

TEST_CASE("sorting")
{
    vector<int> vec(20);
    iota(begin(vec), end(vec), 1);
    
    random_device rd;
    mt19937_64 rnd_gen(rd());
    shuffle(vec.begin(), vec.end(), rnd_gen);

    print(vec, "vec");

    nth_element(begin(vec), begin(vec) + 5, end(vec), greater<>{});
    print(vec, "vec");

    partial_sort(begin(vec), begin(vec) + 5, end(vec));
    print(vec, "vec after partial sort");

    SECTION("partition")
    {
        auto boundary = partition(vec.begin(), vec.end(), [](int x) { return x % 2 == 0;});

        copy(vec.begin(), boundary, ostream_iterator<int>(cout, " "));
        cout << "\n";
        copy(boundary, vec.end(), ostream_iterator<int>(cout, " "));
        cout << "\n";
    }
}