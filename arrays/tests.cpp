#include <algorithm>
#include <array>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>
#include <list>
#include <random>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;
using namespace Utils;

namespace Explain
{
    struct Aggregate
    {
        int x;
        double y;
        int tab[3];
    };

    template <typename T, size_t N>
    struct array
    {
        T items[N];

        //
        using value_type = T;
        using iterator = T*;
        //...

        constexpr iterator begin()
        {
            return &items[0];
        }

        constexpr iterator end()
        {
            return (&items[0] + N);
        }
    };
}

TEST_CASE("Aggregates in C++")
{
    Explain::Aggregate agg1{1, 3.14, {1, 2, 3}};
    Explain::Aggregate agg2{};

    REQUIRE(agg2.x == 0);
    REQUIRE(agg2.y == 0);
    REQUIRE(all_of(begin(agg2.tab), end(agg2.tab), [](int x) { return x == 0; }));
}

std::array<double, 3> create_coord()
{
    return {{1.1, 3.14, 0.1}};
}

struct NoCopyOrMove
{
    std::string text;

    NoCopyOrMove() = delete;
    NoCopyOrMove(const NoCopyOrMove&) = delete;
    NoCopyOrMove(NoCopyOrMove&&) = delete;
};

NoCopyOrMove foo()
{
    return NoCopyOrMove{{"text"}};
}

template <typename T>
struct Wrapper
{
    T obj;

    // Wrapper(T obj) : obj(std::move(obj))
    // {}
};

// deduction guide - since C++17
template <typename T>
Wrapper(T)->Wrapper<T>;

TEST_CASE("Magic C++17")
{
    NoCopyOrMove unbeliveable{}; // error in C++20

    NoCopyOrMove ncm = foo();
    REQUIRE(ncm.text == "text"s);

    Wrapper w{10};
}

TEST_CASE("array")
{
    SECTION("is substitute for static arrays")
    {
        int tab1[10];
        int tab2[10] = {};
        int tab3[10] = {1, 2, 3, 4};

        SECTION("default init creates array with garbage")
        {
            array<int, 10> arr1;
            print(arr1, "arr with default construction");
        }

        SECTION("can be zero initialized")
        {
            array<int, 10> arr = {{}};
            print(arr, "arr");
        }

        SECTION("can be initialized with items")
        {
            array<int, 10> arr1 = {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}};
            print(arr1, "arr1");

            array<int, 10> arr2 = {1, 2, 3, 4, 5};
            print(arr2, "arr2");
        }

        SECTION("can be returned from function")
        {
            array<double, 3> pos = create_coord();

            REQUIRE(pos[0] == Approx(1.1));
            REQUIRE(pos[1] == Approx(3.14));

            SECTION("can be used with structured binding in C++17")
            {
                auto [x, y, z] = create_coord();
                REQUIRE(y == Approx(3.14));

                SECTION("is interpreted as")
                {
                    auto temp = create_coord();
                    auto& x = temp[0];
                    auto& y = temp[1];
                    //..
                }
            }
        }

        SECTION("has deduction guides")
        {
            array arr1{1, 2, 3};
            REQUIRE(arr1.size() == 3);
            REQUIRE(arr1[0] == 1);

            array arr2 = {1, 2, 3, 4};
            REQUIRE(arr2 == array{1, 2, 3, 4});
        }
    }

    SECTION("at()")
    {
        array arr{1, 2, 3};

        REQUIRE_THROWS_AS(arr.at(6), std::out_of_range);
    }
}

namespace LegacyCode
{
    void use_tab(int* tab, unsigned int size)
    {
        cout << "using tab: ";
        for (int* it = tab; it != tab + size; ++it)
            cout << *it << " ";
        cout << '\n';
    }
}

TEST_CASE("working with legacy code")
{
    array arr{1, 2, 3, 4};

    LegacyCode::use_tab(arr.data(), arr.size());
}

TEST_CASE("swap")
{
    array arr1{1, 2, 3, 4};
    array arr2 = {5, 6, 7, 8};

    arr1.swap(arr2);

    REQUIRE(arr1 == array{5, 6, 7, 8});
    REQUIRE(arr2 == array{1, 2, 3, 4});
}

TEST_CASE("tuple interface")
{
    array arr = {1, 2, 3};

    REQUIRE(get<0>(arr) == 1);
    REQUIRE(get<1>(arr) == 2);
    REQUIRE(get<2>(arr) == 3);

    static_assert(tuple_size_v<decltype(arr)> == 3);
}

template <typename Container, typename RndEngine>
void erase_in_random_way(Container& c, RndEngine&& rnd)
{
    while (!c.empty())
    {
        auto index = rnd() % c.size();
        auto pos = next(begin(c), index);
        c.erase(pos);
    }
}

TEST_CASE("ex remove from inside")
{
    random_device rd;
    mt19937_64 rnd_engine{rd()};

    SECTION("vec")
    {
        vector data{1, 2, 3, 4, 5, 6};

        erase_in_random_way(data, rnd_engine);

        REQUIRE(data.empty());
    }

    SECTION("lst")
    {
        list lst{1, 2, 3, 4, 5, 6};

        erase_in_random_way(lst, rnd_engine);
        REQUIRE(lst.empty());
    }
}