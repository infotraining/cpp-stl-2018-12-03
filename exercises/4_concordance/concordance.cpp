#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <algorithm>
#include <chrono>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym. Wyswietl 20 najczęściej występujących slow (w kolejności malejącej).
*/

template<class InputIt, class Size, class UnaryFunction>
InputIt for_each_n(InputIt first, Size n, UnaryFunction f)
{
    for (Size i = 0; i < n; ++first, (void) ++i) {
        f(*first);
 
    }
    return first;
}

int main()
{
    const string file_name = "tokens.txt";

    ifstream fin(file_name);

    if (!fin)
        throw runtime_error("File "s + file_name + " can't be opened");

    auto t1 = chrono::high_resolution_clock::now();

    unordered_map<string, int> concordance(26267); // pair<const string, int>
    concordance.max_load_factor(0.8);

    string word;

    while(fin >> word)
    {
        concordance[word]++;
    }

    auto t2 = chrono::high_resolution_clock::now();

    cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << endl;
    cout << "Buckets count: " << concordance.bucket_count() << endl;

    cout << "Concordance size: " << concordance.size() << endl;

    cout << "Rating:\n";

    multimap<int, string_view, greater<int>> rating;

    for(const auto&[word, counts] : concordance)
        rating.emplace(counts, word);

    for_each_n(begin(rating), 20, [](const auto& rating_pair) { 
        cout << rating_pair.first << " - " << rating_pair.second << endl;
    });

    cout << "\n---- Alt take of rating:\n";

    using ConcordancePair = decltype(concordance)::value_type;

    auto rating_comp = [](const ConcordancePair* a, const ConcordancePair* b) {
        return a->second > b->second;
    };

    multiset<ConcordancePair*, decltype(rating_comp)> rating_alt(rating_comp);

    for(auto& cp : concordance)
        rating_alt.insert(&cp);

    for_each_n(begin(rating_alt), 20, [](const auto& rating_pair) { 
        cout << rating_pair->first << " - " << rating_pair->second << endl;
    });

    cout << "\n---- Alt take of rating (with nodes):\n";

    using ConcordanceNode = decltype(concordance)::node_type;

    auto rating_node_comp = [](const ConcordanceNode& a, const ConcordanceNode& b) {
        return a.mapped() > b.mapped();
    };

    multiset<ConcordanceNode, decltype(rating_node_comp)> rating_nodes_alt(rating_node_comp);

    for(auto it = concordance.begin(); it != concordance.end(); ) 
    {
        auto extraction_pos = it++;
        rating_nodes_alt.insert(concordance.extract(extraction_pos));
    }

    for_each_n(begin(rating_nodes_alt), 20, [](const auto& rating_pair) { 
        cout << rating_pair.key() << " - " << rating_pair.mapped() << endl;
    });
}
