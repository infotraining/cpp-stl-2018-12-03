#include <chrono>
#include <fstream>
#include <iostream>
#include <regex>
#include <set>
#include <string>
#include <vector>
#include <unordered_set>

using namespace std;

vector<std::string> regex_split(const std::string& s, std::string regex_str = R"(\s+)")
{
    std::vector<std::string> words;

    std::regex rgx(regex_str);

    std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
    std::sregex_token_iterator end;

    while (iter != end)
    {
        words.push_back(*iter);
        ++iter;
    }

    return words;
}

int main()
{
    // wczytaj zawartość pliku en.dict ("słownik języka angielskiego")
    // sprawdź poprawość pisowni następującego zdania:
    string input_text = "this is an exmple of very badd snetence";
    vector<string> words = regex_split(input_text);


    ifstream dict_file("en.dict");

    if (!dict_file)
    {
        cerr << "Error opening a file..." << endl;
        exit(1);
    }

    istream_iterator<string> start_dict(dict_file);
    istream_iterator<string> end_dict;

    unordered_set<string> dict(start_dict, end_dict, 100'000);
    
    cout << "Dictionary size: " << dict.size() << endl;

    cout << "Output: ";
    copy_if(begin(words), end(words), ostream_iterator<string>(cout, " "), 
            [&dict](const auto& word) { return !dict.count(word);});
    cout << "\n";

    // //--------------------------------------------
    // auto a_start = dict.lower_bound("a");
    // auto b_start = dict.lower_bound("b");

    // cout << "\a: ";
    // cout << *a_start << " " << *b_start << endl;
    // for(auto it = a_start; it != b_start; ++it)
    //     cout << *it << " ";
    // cout << '\n';
}