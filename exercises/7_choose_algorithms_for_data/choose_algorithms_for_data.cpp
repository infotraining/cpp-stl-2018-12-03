#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <set>
#include <vector>
#include <string>

using namespace std;

template <typename Container>
void print(const Container& container, const string& prefix = "items")
{
    cout << prefix << " : [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    vector<int> vec(35);

    std::random_device rd;
    std::mt19937 rnd_engine{rd()};
    std::uniform_int_distribution<int> uniform_distr{1, 35};

    auto rnd_gen = [&uniform_distr, &rnd_engine] { return uniform_distr(rnd_engine); };

    generate(vec.begin(), vec.end(), rnd_gen);

    vector<int> sorted_vec{vec};
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset{vec.begin(), vec.end()};

    print(vec, "vec        : ");
    print(sorted_vec, "sorted_vec : ");
    print(mset, "mset       : ");

    // check if 17 is in sequence
    cout << "vec         " << (find(begin(vec), end(vec), 17) != end(vec)) << endl;
    cout << "sorted_vec: " << binary_search(begin(sorted_vec), end(sorted_vec), 17) << endl;
    cout << "mset: " << (mset.find(17) != end(mset)) << endl;

    // print number of occurence of 22

    cout << "\nvec         " << count(begin(vec), end(vec), 22) << endl;
    auto [pos1, pos2] = equal_range(begin(sorted_vec), end(sorted_vec), 22);
    cout << "sorted_vec: " <<  (pos2 - pos1) << endl;
    cout << "mset: " << mset.count(22) << endl;

    // remove all numbers greater than 15
    vec.erase(remove_if(begin(vec), end(vec), [](int x) { return x > 15;}), end(vec));
    sorted_vec.erase(upper_bound(begin(vec), end(vec), 15), end(vec));
    mset.erase(mset.upper_bound(15), mset.end());
}
