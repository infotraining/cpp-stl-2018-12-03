#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;
using namespace Catch::Matchers;

TEST_CASE("iterating over container")
{
    // http://quick-bench.com/h1vojuvEEJf0QzltMt-IvD5d3mw

    vector vec = {1, 2, 3};

    for(auto it = begin(vec); it != end(vec); ++it)
        cout << *it << " ";
    cout << "\n";
}



class RangeIterator
{
   int pos_;
public:
   explicit RangeIterator(int pos) : pos_{pos}
   {}

   int operator*() const
   {
       return pos_;
   }

   RangeIterator& operator++() // ++it
   {
       ++pos_;
       return *this;
   }

   RangeIterator operator++(int)  // it++
   {
       RangeIterator tmp{*this};
       ++pos_;
       return tmp;
   }

   bool operator!=(const RangeIterator& other) const
   {
       return pos_ != other.pos_;
   }

   bool operator ==(const RangeIterator& other) const
   {
       return pos_ == other.pos_;
   }
};

class Range
{
    const int start_;
    const int end_;

public:
    Range(int a, int b) : start_{a}, end_{b}
    {}

    RangeIterator begin() const
    {
        return RangeIterator{start_};
    }

    RangeIterator end() const
    {
        return RangeIterator{end_};
    }
};

TEST_CASE("Range")
{
    vector<int> data;

    SECTION("empty range")
    {
        for(const auto& item : Range{1, 1})
        {
            data.push_back(item);
        }

        REQUIRE(data.empty()); 
    }

    SECTION("many items")
    {
        for(const auto& item : Range{1, 10})
        {
            data.push_back(item);
        }

        REQUIRE_THAT(data, Equals(vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9})); 
    }
}