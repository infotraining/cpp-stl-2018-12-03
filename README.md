# README #

## Doc

* https://infotraining.bitbucket.io/cpp-stl

## Proxy settings ##

* add to ``.profile`` file

```
export http_proxy=http://10.158.100.2:8080
export https_proxy=https://10.158.100.2:8080
```

* in cmd line

```
git config --global http.proxy http://10.158.100.2:8080
```

## Celero dependencies ##

* https://github.com/DigitalInBlue/Celero
* ``sudo apt-get install libncurses5-dev``

## apt-get + proxy ##

* ``sudo nano /etc/apt/apt.conf``
* add the folllowing lines:

```
Acquire::http::Proxy "http://10.158.100.2:8080";
Acquire::https::Proxy "https://10.158.100.2:8080";
```

## Benchmarks

* push_backs into containers - http://quick-bench.com/a7BBxiK62CFOb1NUgHhQLkES7ZE
* sorting ints - http://quick-bench.com/6Tu7mSxDSUU-dpOBObHYwVPNNiU
* sorting nomoveable objects - http://quick-bench.com/LDZJ67mM_7JJfYDU5ctIDxF1q6c
* sorting moveable objects - http://quick-bench.com/pHEH74cxbrsNuTlSfYGT9sauhKo

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-stl-2018-12-03-kp